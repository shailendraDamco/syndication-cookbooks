# Cookbook Name:: syndicationcookbook
# Recipe:: default
# Copyright 2014, YOUR_COMPANY_NAME
# All rights reserved - Do Not Redistribute

if node['platform_family'] == "windows"
        include_recipe        "windows"
end

# get the cloud formation information
require('parseconfig.rb')
config = ParseConfig.new('C:\instance\instancedetails.ini')

file "C:\\Example.txt" do
  rights :read, "Everyone"
  rights :full_control, "Everyone"
  action :create
end

# Create PW INI file
# msiexec /i "C:\\syndicationmsi\\SyndicationApplication.msi"
 # msiexec /i "C:\\syndicationmsi\\SyndicationApplication.msi"
 # icacls "D:\\stacks\\websites\\Syndication\\Content\\images" /grant IIS_IUSRS:(OI)(CI)M
 # icacls "D:\\stacks\\websites\\Syndication\\xsl" /grant IIS_IUSRS:(OI)(CI)M

template "#{node['apps']['windows']['drive']}\\#{node['apps']['windows']['stacks-conf']}\\application.ini".gsub('/','\\') do
  action :create
  variables({
     :db_host     =>"#{node['apps']['database']['dbendpoint']}",
     :db_dbname   =>"#{node['apps']['database']['dbname']}",
     :db_user     =>"#{node['apps']['database']['dbuser']}",
     :db_password =>"#{node['apps']['database']['dbpass']}",
     :app_url  =>"#{node['apps']['domains']['name']}" 	 	
  })
end

windows_batch "install-app-releaseft" do
 code <<-EOF
 git clone -b feed "https://LNDeployment:d3pl0ym3nt@bitbucket.org/LNWebDev/syndication.git" "syndicationmsi"
 EOF
end

windows_package "Syndication" do
  action :install
source "C://syndicationmsi//SyndicationApplication.msi"
end

windows_batch "accessRights" do
 code <<-EOF
 icacls "D:\\stacks\\websites\\Syndication\\Content\\images" /grant IIS_IUSRS:(OI)(CI)M
 icacls "D:\\stacks\\websites\\Syndication\\xsl" /grant IIS_IUSRS:(OI)(CI)M
 EOF
end




