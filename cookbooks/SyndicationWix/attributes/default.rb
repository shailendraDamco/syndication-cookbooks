require('parseconfig.rb')
config = ParseConfig.new('C:\instance\instancedetails.ini')

default['apps']['windows']['drive'] = "D:"
default['apps']['windows']['directory'] = "stacks\\websites"
default['apps']['windows']['stacks-conf'] = "stacks\\conf"
default['apps']['windows']['gitsource']=  "https://shailendraDamco:damco#123@bitbucket.org/shailendraDamco/syndication.git"

default['apps']['database']['dbendpoint']=config['syndicationdb']['dbendpoint']
default['apps']['database']['dbname']=config['syndicationdb']['dbname']
default['apps']['database']['dbuser']=config['syndicationdb']['dbuser']
default['apps']['database']['dbpass']=config['syndicationdb']['dbpass']
default['apps']['domains']['name']=config['domains']['FQDN']
default['apps']['releasetags']['name']=config['releasetags']['GitBranch'] 





