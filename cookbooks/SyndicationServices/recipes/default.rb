# Cookbook Name:: syndicationcookbook
# Recipe:: default
# Copyright 2014, YOUR_COMPANY_NAME
# All rights reserved - Do Not Redistribute

if node['platform_family'] == "windows"
        include_recipe        "windows"
end

# get the cloud formation information
require('parseconfig.rb')
require 'net/telnet'
config = ParseConfig.new('C:\instance\instancedetails.ini')


file "C:\\Example.txt" do
  rights :read, "Everyone"
  rights :full_control, "Everyone"
  action :create
end


# Create PW INI file
# "C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\installUtil.exe" /username=.\aws-re-admin /password=UGE%xzmsAg@ /unattended "D:\\stacks\\websites\\Syndication.Wix\\WSKService\\WSKService.exe"
# #git clone -b feed "https://shailendraDamco:damco#123@bitbucket.org/shailendraDamco/syndication.git" "syndicationmsi"
# msiexec /i "C:\\syndicationmsi\\SyndicationServices.msi"

template "#{node['apps']['windows']['drive']}\\#{node['apps']['windows']['stacks-conf']}\\application.ini".gsub('/','\\') do
  action :create
  variables({
     :db_host     =>"#{node['apps']['database']['dbendpoint']}",
     :db_dbname   =>"#{node['apps']['database']['dbname']}",
     :db_user     =>"#{node['apps']['database']['dbuser']}",
     :db_password =>"#{node['apps']['database']['dbpass']}",
     :app_url  =>"#{node['apps']['domains']['name']}" 	 	
  })
end

windows_batch "install-app-releaseft" do
 code <<-EOF  
 git clone -b feed "https://LNDeployment:d3pl0ym3nt@bitbucket.org/LNWebDev/syndication.git" "syndicationmsi"
 EOF
end

windows_package "Syndication" do
  action :install
source "C://syndicationmsi//SyndicationServices.msi"
end

windows_feature "TelnetClient" do
 action :install
end

directory "D:\stacks\websites\Syndication.Wix\WSKService" do
  rights :read, "Everyone"
  rights :full_control, "Everyone"
  action :create
end

windows_batch "RunWSK" do
 code <<-EOF
 "D:\\stacks\\websites\\Syndication.Wix\\WSKService\\WSKService.exe" -install
 EOF
end





