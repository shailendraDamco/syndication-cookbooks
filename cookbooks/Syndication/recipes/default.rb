﻿# -------------------------------------------------------------------------------------------------
#
#     Copyright © 2014 LexisNexis.  All rights reserved.
#     Reed Elsevier (UK) Ltd trading as LexisNexis - Registered in England - Number 2746621.
#     Registered Office 1 - 3 Strand, London WC2N 5JR.
#
# -------------------------------------------------------------------------------------------------
#
# Cookbook Name:: drupal-pensionsworld
# Recipe:: default
#

# Include the Apache recipe
include_recipe 'apache2-windows'

# get the cloud formation information
require('parseconfig.rb')
config = ParseConfig.new('C:\instance\instancedetails.ini')

"#{ENV['SystemDrive']}\\inetpub\\wwwroot\\SyndicationFeedService"
only_if { File.exists?(aspnet_regiis) }

# Create PW INI file

template "#{ENV['SystemDrive']}\\inetpub\\wwwroot\\SyndicationFeedService\\Web.config".gsub('/','\\') do
  action :create
  variables({
     :db_host     =>"#{node['apps']['database']['dbendpoint']}",
     :db_dbname   =>"#{node['apps']['database']['dbname']}",
     :db_user     =>"#{node['apps']['database']['dbuser']}",
     :db_password =>"#{node['apps']['database']['dbpass']}",
	 :app_url	  =>"#{node['apps']['domains']['name']}" 	 	
  })
end


end
