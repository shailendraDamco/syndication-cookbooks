require('parseconfig.rb')
config = ParseConfig.new('C:\instance\instancedetails.ini')

default['apps']['windows']['drive'] = "D:"
default['apps']['windows']['directory'] = "stacks\\websites"
default['apps']['windows']['stacks-conf'] = "stacks\\conf"
default['apps']['windows']['gitsource'] = "https://LNDeployment:d3pl0ym3nt@bitbucket.org/LNWebDev/pensionsworld_drupal6.git"

default['apps']['database']['dbendpoint']=config['Pensionworld']['dbendpoint']
default['apps']['database']['dbname']=config['Pensionworld']['dbname']
default['apps']['database']['dbuser']=config['Pensionworld']['dbuser']
default['apps']['database']['dbpass']=config['Pensionworld']['dbpass']
default['apps']['domains']['name']=config['domains']['Pensionworld']
default['apps']['releasetags']['name']=config['releasetags']['PensionworldFrontEnd'] 
