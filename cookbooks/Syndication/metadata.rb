name             'drupal-pensionsworld'
maintainer       'Damco Group'
maintainer_email 'ahmads@damcogroup.com'
license          'All rights reserved'
description      'Installs/Configures drupal-pensionsworld'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.57'
