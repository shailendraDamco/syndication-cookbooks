name             'GetInstance2IP'
maintainer       'LexisNexisUK'
maintainer_email 'loveleshs@damcogroup.com'
license          'All rights reserved'
description      'Installs/Configures CDriveTagging for all AWS EC2 Instances'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
supports         "windows"
depends          "windows"