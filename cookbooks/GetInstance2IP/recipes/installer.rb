

#
# Cookbook Name:: CDriveTagging
# Recipe:: installer
#
# Copyright 2014, LexisNexisUK
#
# All rights reserved - Do Not Redistribute
#

#For installing CDriveTagging Prerequisites
directory 'C:/cdrivetagging' do
  action :create
end

windows_package "Python 2.7" do
  source "https://s3-eu-west-1.amazonaws.com/cdrivetagging/python-2.7.6.amd64.msi"
  installer_type :msi
  action :install
  timeout 6000
end

remote_file "C:/cdrivetagging/boto-2.24.0.tar.gz" do
  source "https://s3-eu-west-1.amazonaws.com/cdrivetagging/boto-2.24.0.tar.gz"
end

template "c:/cdrivetagging/extractTARGZ.py" do
source "extractTARGZ.py.erb"
action :create
end


batch "extract_and_install_boto" do
  code <<-EOH
    C:/Python27/python.exe c:/cdrivetagging/extractTARGZ.py
    EOH
end



