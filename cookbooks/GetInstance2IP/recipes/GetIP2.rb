

#
# Cookbook Name:: CDriveTagging
# Recipe:: tagger
#
# Copyright 2014, LexisNexisUK
#
# All rights reserved - Do Not Redistribute
#

#For creating a CDriveTagger and cleaning the installed packages
require('parseconfig.rb')




template "c:/cdrivetagging/getip.py" do
source "getip.py.erb"
action :create
end

batch "extract_and_install_boto" do
  code <<-EOH
    C:/Python27/python.exe c:/cdrivetagging/getip.py
    EOH
end

