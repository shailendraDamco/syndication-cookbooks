##############################################################
########### Apache installation ##########################
windows_package "Apache 2.2.25" do
  source "http://apache.mirror.anlx.net//httpd/binaries/win32/httpd-2.2.25-win32-x86-openssl-0.9.8y.msi"
  installer_type :custom
  options 'INSTALLDIR=C:\Apache2 /q'
end

##############################################################
########### GIT Client installation ##########################
windows_package "GIT" do
  source "https://msysgit.googlecode.com/files/Git-1.8.4-preview20130916.exe"
  action :install
end
#### env variables for GIT
windows_path 'C:\\Program Files (x86)\\Git\\bin' do
  action :add
end

##############################################################
########## Mysql Client installation ##########################
windows_package "Mysql 5.0" do
source "http://downloads.mysql.com/archives/mysql-5.1/mysql-essential-5.1.71-win32.msi"
  installer_type :custom
  options 'INSTALLDIR=C:\MYSQL /q'
end
#### Env Var for mysql
windows_path 'c:\\MYSQL\\bin' do
  action :add
end

#windows_package "mysql" do
#  source "http://dev.mysql.com/get/Downloads/MySQLInstaller/mysql-installer-community-5.6.14.0.msi/from/http://cdn.mysql.com/"
#  action :install
#end

#windows_package "PHP 5.3.27" do
#  source "http://windows.php.net/downloads/releases/php-5.3.27-nts-Win32-VC9-x86.msi"
#  installer_type :msi
#  action :install
#end

#windows_package "Python 2.7" do
#source "http://www.python.org/ftp/python/2.7/python-2.7.msi"
#  installer_type :msi
#  action :install
#end

################# Directory Hierarchy #####################
directory 'c:\stacks' do
rights :full_control, 'aws-re-admin'
action :create
#inherits false
end

%w{websites conf logs tmp bundles}.each do |dir|
    directory "c:/stacks/#{dir}" do
    rights :full_control, 'aws-re-admin'
    action :create
#    recursive true
  end
end
##########################################################

